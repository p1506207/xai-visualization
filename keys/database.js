/**
 * File use to create the token to request API
 */

let fs = require('fs');
let jwt = require('jsonwebtoken');

var privateKey = fs.readFileSync('jwt_plateform.key');

let payload = {
    user: "asker:ext_Benoit.Riandiere",
    role: "learner",
    username: "ext_Benoit.Riandiere",
    objectives: [["Manipuler_les_paramètres_dun_script", "Revision"], 
               ["Substituer_une_commande_par_son_exécution", "Revision"], 
               ["Réaliser_des_calculs_arithmétiques", "Revision"]],

    fwid: 83,
    exp: 20
};

let token = jwt.sign(payload, privateKey, { algorithm: 'RS256'});
console.log(token);