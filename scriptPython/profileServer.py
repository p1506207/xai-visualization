#!/usr/bin/python3
# -*- coding: utf-8 -*-

import traceback
import requests
import jwt, base64
import flask
import json
import flask_cors

app = flask.Flask(__name__)

@app.route("/api/profile/recommendation/index.php", methods=['GET'])
@flask_cors.cross_origin()
def profile():

	frameworkId = flask.request.args.get('frameworkId')
	learnerUsername = flask.request.args.get('learnerUsername')
	print("Profile for user", learnerUsername , "in referential #"+str(frameworkId))
	
	f = open("./profile.json", "r")
	data = json.load(f)
	f.close()

	return data, 200, {'Content-Type':'application/json'}

@app.route("/api/logs/last/", methods=['GET'])
@flask_cors.cross_origin()
def getLastLogFile():
	print()
	global my_arguments, referential

	return flask.send_file('./trace_revision.txt', attachment_filename='trace.log')

if __name__ == '__main__':
	app.run("127.0.0.1", 5000, debug=True)