define(["require", "exports", "./framework"], function (require, exports, framework_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.formatNodeName = exports.formatValue = exports.getTooltipTextForArrow = void 0;
    // Get the text to display in the tooltip depending of the relation 
    function getTooltipTextForArrow(childNode, parentNode, relation, childPathNode, linkColor) {
        let tooltipText = '';
        // Update of the content of the tooltip
        let childName = formatNodeName(childNode.getName()).toLowerCase();
        let parentName = formatNodeName(parentNode.getName()).toLowerCase();
        // Fix the synthax error "de le"
        let transitionText = '';
        let objectifText = '';
        if (parentNode.getClassNode() === framework_1.ClassNode.objectif)
            objectifText = 'objectif';
        if (!childNode.isRessource() && (parentNode.getTypeNode() === framework_1.TypeNode.skill || parentNode.getTypeNode() === framework_1.TypeNode.learning)) {
            let textForRelation = getTooltipTextForRelation(relation);
            transitionText = textForRelation.substring(0, textForRelation.length - 1)
                + 'u</span> '
                + getTooltipTextForTypeNode(parentNode.getTypeNode()).substring(3)
                + ' '
                + objectifText
                + '<i style="font-weight:100;"> ' + parentName + '</i>.</div>';
        }
        else {
            transitionText = getTooltipTextForRelation(relation)
                + '</span> '
                + getTooltipTextForTypeNode(parentNode.getTypeNode())
                + ' '
                + objectifText
                + '<i style="font-weight:100;"> ' + parentName + '</i>.</div>';
        }
        // Make the first letter to uppercase
        let childType = getTooltipTextForTypeNode(childNode.getTypeNode());
        let childTypeWithMaj = childType[0].toUpperCase() + childType.substr(1);
        tooltipText = '<div>'
            + childTypeWithMaj
            + '<i style="font-weight:100;"> ' + childName + '</i> '
            + '<span style="color:' + linkColor + ';font-weight:900;"> '
            + transitionText;
        // Display the cause of the selection of the node if the node isn't a resource
        if (!childNode.isRessource()) {
            tooltipText += getCauseOfSelection(childPathNode, childNode);
        }
        return tooltipText;
    }
    exports.getTooltipTextForArrow = getTooltipTextForArrow;
    // Get the text to display in the second part of the tooltip depending of the selection strategie
    // For more information about this, check the details of strategie.
    function getCauseOfSelection(childPathNode, childNode) {
        let isFemaleType = isFemaleTypeNode(childNode.getTypeNode());
        let tooltipText = '</br><div>'
            + getTooltipText2ForTypeNode(childNode.getTypeNode())
            + ' vous est recommandé';
        if (isFemaleType)
            tooltipText += 'e';
        tooltipText += ' car ';
        switch (childPathNode.getSelectionStrategie()) {
            case (framework_1.SelectionStrategie.RS1):
                if (childNode.getTrust() > 0) {
                    tooltipText += 'vous ne l';
                    if (isFemaleType)
                        tooltipText += 'a';
                    else
                        tooltipText += 'e';
                    tooltipText += ' maîtrisez pas encore suffisamment';
                }
                else {
                    tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                    if (isFemaleType)
                        tooltipText += 'e';
                }
                break;
            case (framework_1.SelectionStrategie.RS2):
                if (childNode.getTrust() > 0) {
                    tooltipText += 'vous ne l';
                    if (isFemaleType)
                        tooltipText += 'a';
                    else
                        tooltipText += 'e';
                    tooltipText += ' maîtrisez pas encore suffisamment';
                }
                else {
                    tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                    if (isFemaleType)
                        tooltipText += 'e';
                }
                break;
            case (framework_1.SelectionStrategie.RS3):
                if (childNode.getTrust() > 0) {
                    if (childPathNode.getTag() === framework_1.Tag.revision) {
                        tooltipText += 'vous ne l';
                        if (isFemaleType)
                            tooltipText += 'a';
                        else
                            tooltipText += 'e';
                        tooltipText += ' maîtrisez pas encore suffisamment';
                    }
                    else {
                        tooltipText += 'cela peut vous aider à réviser';
                    }
                }
                else {
                    tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                    if (isFemaleType)
                        tooltipText += 'e';
                }
                break;
            case (framework_1.SelectionStrategie.RS4):
                // It shouldn't be true
                if (childNode.getTrust() > 0) {
                    tooltipText += 'vous ne l';
                    if (isFemaleType)
                        tooltipText += 'a';
                    else
                        tooltipText += 'e';
                    tooltipText += ' maîtrisez pas encore suffisamment';
                }
                else {
                    tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                    if (isFemaleType)
                        tooltipText += 'e';
                }
                break;
            case (framework_1.SelectionStrategie.RS5):
                if (childNode.getTrust() > 0) {
                    tooltipText += 'vous ne l';
                    if (isFemaleType)
                        tooltipText += 'a';
                    else
                        tooltipText += 'e';
                    tooltipText += ' maîtrisez pas encore suffisamment';
                }
                else {
                    tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                    if (isFemaleType)
                        tooltipText += 'e';
                }
                break;
            // It shouldn't appeared
            case (framework_1.SelectionStrategie.RS6):
                if (childNode.getTrust() > 0) {
                    tooltipText += 'vous ne l';
                    if (isFemaleType)
                        tooltipText += 'a';
                    else
                        tooltipText += 'e';
                    tooltipText += ' maîtrisez pas encore suffisamment';
                }
                else {
                    tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                    if (isFemaleType)
                        tooltipText += 'e';
                }
                break;
        }
        tooltipText += '.</div>';
        return tooltipText;
    }
    // Get the text to display in the tooltip depending of the relation 
    function getTooltipTextForRelation(relation) {
        switch (relation) {
            case (framework_1.Relation.hasSkill):
            case (framework_1.Relation.isSkillOf):
            case (framework_1.Relation.hasKnowledge):
            case (framework_1.Relation.isKnowledgeOf):
            case (framework_1.Relation.composes):
            case (framework_1.Relation.isComposedOf):
            case (framework_1.Relation.comprises):
            case (framework_1.Relation.isComprisedIn):
                return 'est un composant de';
            case (framework_1.Relation.complexifies):
            case (framework_1.Relation.isComplexificationOf):
                return 'est une complexification de';
            case (framework_1.Relation.facilitatesUnderstandingOf):
            case (framework_1.Relation.isLeverOfUnderstandingOf):
                return 'est un levier de compréhension de';
            case (framework_1.Relation.requires):
            case (framework_1.Relation.isRequiredBy):
                return 'est un prérequis de';
            case (framework_1.Relation.hasLearning):
            case (framework_1.Relation.isLearningOf):
            case (framework_1.Relation.hasTraining):
            case (framework_1.Relation.isTrainingOf):
                return 'permet de travailler';
            default:
                return relation;
        }
    }
    // Get the text to display in the tooltip depending of the type of the node 
    function getTooltipTextForTypeNode(type) {
        switch (type) {
            case (framework_1.TypeNode.competency):
                return "la compétence";
            case (framework_1.TypeNode.knowledge):
                return "la connaissance";
            case (framework_1.TypeNode.skill):
                return "le savoir-faire";
            case (framework_1.TypeNode.learning):
                return "le cours";
            case (framework_1.TypeNode.training):
                return "l\'exercice";
            default:
                return "";
        }
    }
    // Return true if the type is female gender for the synthax
    function isFemaleTypeNode(type) {
        switch (type) {
            case (framework_1.TypeNode.competency):
            case (framework_1.TypeNode.knowledge):
                return true;
            case (framework_1.TypeNode.skill):
                return false;
            default:
                return false;
        }
    }
    // Get the text to display in the tooltip depending of the type of the node 
    function getTooltipText2ForTypeNode(type) {
        switch (type) {
            case (framework_1.TypeNode.competency):
                return "Cette compétence";
            case (framework_1.TypeNode.knowledge):
                return "Cette connaissance";
            case (framework_1.TypeNode.skill):
                return "Ce savoir-faire";
            default:
                return "";
        }
    }
    // Convert number to percentage
    function formatValue(value) {
        if (isNaN(value) || value === undefined || value === null)
            value = 0;
        return Math.round(value * 100).toString() + '%';
    }
    exports.formatValue = formatValue;
    // Convert name from BD to a propre text
    function formatNodeName(nodeName) {
        return formatResourceName(nodeName).replace(new RegExp("_", "g"), ' ')
            .replace(new RegExp("de une", "g"), 'd\'une')
            .replace(new RegExp("dune", "g"), 'd\'une')
            .replace(new RegExp("dun", "g"), 'd\'un')
            .replace(new RegExp("de un", "g"), 'd\'un');
    }
    exports.formatNodeName = formatNodeName;
    //Exclude the prefix the name of the resource
    function formatResourceName(oldName) {
        let newName = oldName.split(" - ", 2);
        return newName[newName.length - 1];
    }
});
