var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
define(["require", "exports", "./framework"], function (require, exports, framework_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Traces = exports.requestTracesAPI = void 0;
    function requestTracesAPI() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield fetchTraces();
            }
            catch (error) {
                console.log("Error fetching remote HTML: ", error);
            }
        });
    }
    exports.requestTracesAPI = requestTracesAPI;
    /**
     * @returns a promise of the trace of recommandations.
     */
    const fetchTraces = () => __awaiter(void 0, void 0, void 0, function* () {
        //for test --> TO ERASE
        const url = 'http://127.0.0.1:5000';
        const route = '/api/logs/last/';
        //const api = "https://traffic.irit.fr/comper/recommendations/api/logs/last/";
        const api = url + route;
        var myHeaders = new Headers();
        myHeaders.append("Access-Control-Allow-Origin", "*");
        myHeaders.append("Access-Control-Allow-Methods", "POST, GET");
        myHeaders.append("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYXNrZXI6ZXh0X0Jlbm9pdC5SaWFuZGllcmUiLCJyb2xlIjoibGVhcm5lciIsInVzZXJuYW1lIjoiZXh0X0Jlbm9pdC5SaWFuZGllcmUiLCJvYmplY3RpdmVzIjpbWyJNYW5pcHVsZXJfbGVzX3BhcmFtw6h0cmVzX2R1bl9zY3JpcHQiLCJSZXZpc2lvbiJdLFsiU3Vic3RpdHVlcl91bmVfY29tbWFuZGVfcGFyX3Nvbl9leMOpY3V0aW9uIiwiUmV2aXNpb24iXSxbIlLDqWFsaXNlcl9kZXNfY2FsY3Vsc19hcml0aG3DqXRpcXVlcyIsIlJldmlzaW9uIl1dLCJmd2lkIjo4MywiZXhwIjoyMCwiaWF0IjoxNjI0NjIyMzMwfQ.FlXyrS3KhdFqfZwo38Lo_g2ipIKCKxGLk4IFeIXmr2Ph2jBO0GrNnteH7tp-3wu6YvXGnwIOl0U9LMJxX64SCeBm5JEIyaTvpxWYQanG9fcIuV0GDmE8ZSQK_EOQ0VvdswkWAqHNd0ZaR2QqfQhZfMJ6ywE2CU2OPfcFk-fK-MbUM9x1x5eHvqr--KCn7PR-OIl3EcHmWtNFCW2OPjKu-WvVsdomrF_9dInT9VhKHcBZUSbfnujAtkmswdXQWdHlR6a8dqU2YrLEpYxwbk4AGjugvFm5AapGt-0suVHaemcoe-UEDMx95clrC38VUqKAfPoDIUeD2GijZc2u7vRNqDvh2QSlJOXYPbiPzBH83oc3kUm6bDEXPbISGeLEIpFgL0tOI7l-2lwHXLo12dpDyPdtZDIuxhaGvkXLHzWCeG3ZoX0CFc61QaTZJGr9ish1x-jZtihJAWmbzI0zsYAxgV62H21sHxBaEmIcbEaavI9-uaWjH_zA_y6U1pqvh_nJoQ3UZXtAsiXviUKnelE61WWda12IqhwSDwpMuk40WVUlDkxNRBTgx-AgJwy1Zl-QNdioU8tbXZ7uroNtUWNtXd8lw27GpgBUIK3tTh1xsHuLE6KXc9HfOa4jhQbS4gem9WRls4SgpysNbrhfaRsGim07CLE3mHDp-ss9EcYxLWk");
        myHeaders.append("Accept", "text/plain");
        var myInit = { method: 'GET',
            headers: myHeaders };
        var myRequest = new Request(api, myInit);
        try {
            const response = yield fetch(myRequest, myInit);
            const data = yield response.text();
            return data;
        }
        catch (error) {
            if (error) {
                return error.message;
            }
        }
    });
    /**
     * Contains data get from traces
     */
    class Traces {
        constructor(tracesFile, objectifNum) {
            /**
             * Each bloc variable contains a list of string associate to the the bloc in the trace
             */
            this.blocReferentiel = [];
            this.blocProfil = [];
            this.blocObjectif = [];
            this.blocsSelectionKSC = new Map();
            this.blocUnicity = [];
            this.blocOrdering = [];
            this.blocSelectionResources = [];
            this.blocOrderingResources = [];
            this.blocRestricting = [];
            this.blocOut = [];
            this.setBlocs(tracesFile, objectifNum);
            this.setRecommandationsPath(objectifNum);
        }
        getObjectif() {
            return this.objectif;
        }
        getRecommandationsPath() {
            return this.recommandationsPath;
        }
        /**
         * Use traces to generate paths between each recommandation and the objectif
         * @param objectifNum the objectif we want explanation
         */
        setRecommandationsPath(objectifNum) {
            this.recommandationsPath = [];
            let path = [];
            let blocSelectionKSC = this.blocsSelectionKSC.get(objectifNum);
            // For each recommandation
            this.blocOut.forEach((recommandation) => {
                let decompose = recommandation.split("'");
                let ids = decompose[2].substring(0, decompose[2].length - 1).split(",");
                let idRecommandation = decompose[1];
                let idParent = parseInt(ids[1].substring(1));
                let idObjectif = parseInt(ids[2].substring(1));
                // If it correspond to the objectif treated
                if (idObjectif === this.idObjectif) {
                    path = [];
                    path.push([idRecommandation, null, null, null]);
                    // Find if the resource is an exercise or a course to get the appropriate relation
                    let relation = "";
                    this.blocOrderingResources.forEach((line) => {
                        if (line.includes(idRecommandation)) {
                            let link = line.split("'")[3];
                            if (link = "expositive")
                                relation = "hasLearning";
                            else
                                relation = "hasTraining";
                        }
                    });
                    // Iterate over the output of the selection phase to get the tag and weight
                    let i = blocSelectionKSC.length - 1;
                    let bestWeight = 0;
                    let parentName = "";
                    while (blocSelectionKSC[i].includes("Output")) {
                        let stringFragment = blocSelectionKSC[i].split("'");
                        let idNode = parseInt(stringFragment[4].substring(3, stringFragment[4].length - 2));
                        if (idNode === idParent) {
                            let weight = parseFloat(stringFragment[12].substring(2, stringFragment[12].length - 1));
                            if (weight > bestWeight) {
                                bestWeight = weight;
                                parentName = stringFragment[5];
                            }
                        }
                        i--;
                    }
                    // Iterate over the selection phase to get the path
                    while (parentName !== this.objectif) {
                        if (i < 0) {
                            break;
                        }
                        if (blocSelectionKSC[i].includes("[treatment][") && blocSelectionKSC[i].includes("][selection]")) {
                            let stringFragment = blocSelectionKSC[i].split("'");
                            // Check if it's the good selection of the parent
                            if (stringFragment[1] !== parentName && stringFragment[5] === parentName) {
                                // && parseFloat(stringFragment[8].substring(2, stringFragment[8].length - 2)) === bestWeight
                                let selectionRule = blocSelectionKSC[i].split("][")[3];
                                let tag = stringFragment[11];
                                path.push([parentName, getRelation(relation), getTag(tag), getSelectionStrategie(selectionRule)]);
                                // Go to his selection to get his father and his relation with him
                                while (i >= 0) {
                                    if (!blocSelectionKSC[i].includes("][preTreatment][output]")) {
                                        i--;
                                    }
                                    else if (blocSelectionKSC[i].split("'")[5] !== parentName) {
                                        i--;
                                    }
                                    else {
                                        break;
                                    }
                                }
                                stringFragment = blocSelectionKSC[i].split("'");
                                relation = stringFragment[13];
                                parentName = stringFragment[17];
                                // Go to his selection to get his father and his relation with him
                                while (i >= 0 && blocSelectionKSC[i].split("'")[1] !== parentName) {
                                    stringFragment = blocSelectionKSC[i].split("'");
                                    if (blocSelectionKSC[i].includes("][initialisation]") && stringFragment[5] === parentName && blocSelectionKSC[i].includes("father")) {
                                        let selectionRule = blocSelectionKSC[i].split("][")[3];
                                        path.push([parentName, getRelation(relation), null, getSelectionStrategie(selectionRule)]);
                                        relation = framework_1.Relation[framework_1.getSymetricRelation(getRelation(stringFragment[13]))];
                                        parentName = stringFragment[17];
                                    }
                                    i--;
                                }
                            }
                        }
                        i--;
                    }
                    path.push([parentName, getRelation(relation), null, null]);
                    this.recommandationsPath.push(path);
                }
            });
        }
        /**
         * Set bloc lines in class variables
         * @param tracesFile the traces just get from API
         * @param objectifNb the objectif we want explanation
         */
        setBlocs(tracesFile, objectifNum) {
            let tracesLines = tracesFile.split("\n");
            let line = 0;
            let nbObjectif = 0;
            let currentObjectif = 1;
            // Browse the trace file
            while (line < tracesLines.length) {
                // blank
                if (tracesLines[line] === "\n" && this.blocObjectif.length !== 0 && this.blocsSelectionKSC.get(currentObjectif).length !== 0)
                    currentObjectif++;
                // bloc : framework
                else if (tracesLines[line].includes("[Referential]"))
                    this.blocReferentiel.push(tracesLines[line]);
                // bloc : profil
                else if (tracesLines[line].includes("[Profil]"))
                    this.blocProfil.push(tracesLines[line]);
                // bloc : Objectif
                else if (tracesLines[line].includes("[Objective]")) {
                    this.blocObjectif.push(tracesLines[line]);
                    nbObjectif++;
                    this.blocsSelectionKSC.set(nbObjectif, []);
                    // blocs : Selection of KSC
                }
                else if (tracesLines[line].includes("[Selection][KSC]")) {
                    if (tracesLines[line].includes("[Unicity]"))
                        this.blocUnicity.push(tracesLines[line]);
                    else
                        this.blocsSelectionKSC.get(currentObjectif).push(tracesLines[line]);
                    // blocs : Ordering
                }
                else if (tracesLines[line].includes("[Ordering][KSC]"))
                    this.blocOrdering.push(tracesLines[line]);
                // blocs : Selection resources
                else if (tracesLines[line].includes("[Selection][resources]"))
                    this.blocSelectionResources.push(tracesLines[line]);
                // blocs : Ordering resources
                else if (tracesLines[line].includes("[Ordering][resources]"))
                    this.blocOrderingResources.push(tracesLines[line]);
                // blocs : Restricting
                else if (tracesLines[line].includes("[Restricting][resources]")) {
                    if (tracesLines[line].includes("[out]"))
                        this.blocOut.push(tracesLines[line]);
                    else
                        this.blocRestricting.push(tracesLines[line]);
                }
                line++;
            }
            // Set the objectif depending of the objectif number
            this.objectif = this.blocObjectif[objectifNum - 1].split("'")[5];
            let idFragment = this.blocObjectif[objectifNum - 1].split("'")[4];
            this.idObjectif = parseInt(idFragment.substring(3, idFragment.length - 2));
        }
    }
    exports.Traces = Traces;
    function getRelation(relation) {
        let key = relation;
        return framework_1.Relation[key];
    }
    function getTag(tag) {
        switch (tag) {
            case ("Tag_prerequis"):
                return framework_1.Tag.prerequis;
            case ("Tag_remediation"):
                return framework_1.Tag.remediation;
            case ("Tag_renforcement"):
                return framework_1.Tag.renforcement;
            case ("Tag_revision"):
                return framework_1.Tag.revision;
            case ("Tag_decouverte"):
                return framework_1.Tag.découverte;
            case ("Tag_approfondissement"):
                return framework_1.Tag.approfondissement;
            case ("Tag_a_travailler"):
                return framework_1.Tag.aTravailler;
            default:
                return null;
        }
    }
    function getSelectionStrategie(selectionRule) {
        switch (selectionRule) {
            case ("SP1"):
                return framework_1.SelectionStrategie.RS1;
            case ("SP2"):
                return framework_1.SelectionStrategie.RS2;
            case ("SP3"):
                return framework_1.SelectionStrategie.RS3;
            case ("SP4"):
                return framework_1.SelectionStrategie.RS4;
            case ("SP5"):
                return framework_1.SelectionStrategie.RS5;
            case ("SP6"):
                return framework_1.SelectionStrategie.RS6;
            default:
                return null;
        }
    }
});
