var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.requestProfilAPI = void 0;
    function requestProfilAPI(fwId, username) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield fetchProfil(fwId, username);
            }
            catch (error) {
                console.log("Error fetching remote HTML: ", error);
            }
        });
    }
    exports.requestProfilAPI = requestProfilAPI;
    /**
     * @returns a promise of the profil of the student.
     */
    const fetchProfil = (fwId, username) => __awaiter(void 0, void 0, void 0, function* () {
        //for test --> TO ERASE
        const url = 'http://127.0.0.1:5000';
        //real url. Currently, need to get the authorization here before request:
        //https://cors-anywhere.herokuapp.com/corsdemo
        //const url :string = 'https://cors-anywhere.herokuapp.com/https://comper.projet.liris.cnrs.fr/sites/profile-engine';
        const route = '/api/profile/recommendation/index.php';
        const parameter1 = '?frameworkId=' + fwId;
        const parameter2 = '&learnerUsername=' + username;
        const parameters = parameter1 + parameter2;
        const api = url + route + parameters;
        var myHeaders = new Headers();
        myHeaders.append("Access-Control-Allow-Origin", "*");
        myHeaders.append("Access-Control-Allow-Methods", "POST, GET");
        myHeaders.append("Access-Control-Allow-Headers", "x-comper-accepted-host");
        myHeaders.append("x-comper-accepted-host", "https://traffic.irit.fr");
        var myInit = { method: 'GET',
            headers: myHeaders };
        var myRequest = new Request(api, myInit);
        try {
            const response = yield fetch(myRequest, myInit);
            const data = yield response.json();
            return data;
        }
        catch (error) {
            if (error) {
                return error.message;
            }
        }
    });
});
