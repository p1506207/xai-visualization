define(["require", "exports", "./framework"], function (require, exports, framework_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.getTooltipTextForArrow = void 0;
    // Get the text to display in the tooltip depending of the relation 
    function getTooltipTextForArrow(childNode, parentNode, relation, linkColor) {
        let tooltipText = '';
        // Update of the content of the tooltip
        let childName = this.formatNodeName(childNode.getName()).toLowerCase();
        let parentName = this.formatNodeName(parentNode.getName()).toLowerCase();
        // Fix the synthax error "de le"
        let transitionText = '';
        let objectifText = '';
        if (parentNode.getClassNode() === framework_1.ClassNode.objectif)
            objectifText = 'objectif';
        if (!childNode.isRessource() && (parentNode.getType() === framework_1.TypeNode.skill || parentNode.getType() === framework_1.TypeNode.learning)) {
            let textForRelation = this.getTooltipTextForRelation(relation);
            transitionText = textForRelation.substring(0, textForRelation.length - 1)
                + 'u</span> '
                + this.getTooltipTextForTypeNode(parentNode.getType()).substring(3)
                + ' '
                + objectifText
                + '<i style="font-weight:100;"> ' + parentName + '</i>.</div>';
        }
        else {
            transitionText = this.getTooltipTextForRelation(relation)
                + '</span> '
                + this.getTooltipTextForTypeNode(parentNode.getType())
                + ' '
                + objectifText
                + '<i style="font-weight:100;"> ' + parentName + '</i>.</div>';
        }
        // Make the first letter to uppercase
        let childType = this.getTooltipTextForTypeNode(childNode.getType());
        let childTypeWithMaj = childType[0].toUpperCase() + childType.substr(1);
        tooltipText = '<div>'
            + childTypeWithMaj
            + '<i style="font-weight:100;"> ' + childName + '</i> '
            + '<span style="color:' + linkColor + ';font-weight:900;"> '
            + transitionText;
        // Display the rate of the child node if the user got low mastery on it
        if (!childNode.isRessource() && childNode.getTrust() > 0 && childNode.getMastery() < 0.7) {
            tooltipText += '</br></div>'
                + '<div>Taux de maîtrise actuel: '
                + '<span style="color:red">' + this.formatValue(childNode.getMastery()) + '</span>'
                + '</div><div>Taux minimum nécessaire: 70%</div>';
        }
        return tooltipText;
    }
    exports.getTooltipTextForArrow = getTooltipTextForArrow;
    // Get the text to display in the tooltip depending of the relation 
    function getTooltipTextForRelation(relation) {
        switch (relation) {
            case (framework_1.Relation.hasSkill):
            case (framework_1.Relation.isSkillOf):
            case (framework_1.Relation.hasKnowledge):
            case (framework_1.Relation.isKnowledgeOf):
            case (framework_1.Relation.composes):
            case (framework_1.Relation.isComposedOf):
            case (framework_1.Relation.comprises):
            case (framework_1.Relation.isComprisedIn):
                return 'est un composant de';
            case (framework_1.Relation.complexifies):
            case (framework_1.Relation.isComplexificationOf):
                return 'est une complexification de';
            case (framework_1.Relation.facilitatesUnderstandingOf):
            case (framework_1.Relation.isLeverOfUnderstandingOf):
                return 'est un levier de compréhension de';
            case (framework_1.Relation.requires):
            case (framework_1.Relation.isRequiredBy):
                return 'est un prérequis de';
            case (framework_1.Relation.hasLearning):
            case (framework_1.Relation.isLearningOf):
            case (framework_1.Relation.hasTraining):
            case (framework_1.Relation.isTrainingOf):
                return 'permet de travailler';
            default:
                return relation;
        }
    }
    // Get the text to display in the tooltip depending of the type of the node 
    function getTooltipTextForTypeNode(type) {
        switch (type) {
            case (framework_1.TypeNode.competency):
                return "la compétence";
            case (framework_1.TypeNode.knowledge):
                return "la connaissance";
            case (framework_1.TypeNode.skill):
                return "le savoir-faire";
            case (framework_1.TypeNode.learning):
                return "le cours";
            case (framework_1.TypeNode.training):
                return "l\'exercice";
            default:
                return "";
        }
    }
});
