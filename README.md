# XAI-Visualization

Ce projet consiste à créer une visualisation permettrant d'expliquer la manière dont ont été générée les recommandations de l'application Asker.
Pour ce faire je suis parti de la visualisation créée par Rémy Casado disponble à cette adresse: https://gitlab.com/RemiCasado/open-learner-model.

## Execution

Une fois téléchargé il suffit d'ouvrir le fichier 'index.html' dans le navigateur de votre choix.
J'ai créer une version test pour une présentation à venir. Dans celle-ci, pour que cela fonctionne, il faut lancer un serveur local avant de pouvoir utiliser l'application.
Pour cela, il faut se rendre dans le dossier "/scriptPython", et ouvrir une invite de commande pour exécuter la commande "python profileServer.py"

## Dépendances

Ce projet a été développé en Typescript et utilise le framework Bootstrap et la bibliothèque D3.
Au niveau de la version test, le script python a besoin de certaines bibliothèque pour fonctionner à savoir : traceback, requests, jwt, base64, flask, json et flask_cors.

## À savoir sur le code

Pour le développement de cette application, elle a été réalisée en Typescript, donc tout le code se trouve dans les fichiers du répertoire "/src/*.ts".
Une fois le code terminé, il faut le build, ce qui va automatiquement créer les fichiers javascript associés dans le répertoire "/js/libs/*.js" qui seront les fichiers interprêtés à l'exécution.

Cette visualisation se base sur 2 types de données:
 * le profil de l'utilisateur concerné par les explications que je récupère dans le fichier "/src/profil.ts", en requêtant une API.
  * pour la version test ce profil est récupèré du serveur local.
  * pour la suite il faut le requêter à cette adresse : "https://comper.projet.liris.cnrs.fr/sites/profile-engine". Tous les paramètres de la requêtes sont déjà générés, cependant en travaillant en local il y a un problème de CORS qui a été contourné avec l'aide d'un serveur intermédiaire, qu'il faudra remplacer une fois intégrer sur l'application final.

 * les traces générées par l'algorithme. Ces traces sont récupèrés en requêtant une API. 
 /!\ Leur exploitation a été réalisé dans le fichier "/src/traces.ts" mais étant donné qu'elles sont sous forme de string, la moindre modifications du format des traces entraînera des erreurs.
 Il faudra donc modifier ce fichier dans le cas d'une modification des traces.
