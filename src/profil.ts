
export interface FrameworkObjectBD {
  name      :string,
  objects   :Array<FrameworkNodeBD>,
  resources :Array<FrameworkResourceBD>
}

export interface FrameworkResourceBD {
  id                     :string,
  name                   :string,
  interactivityType      :string,
  learningResourceType   :string,
  significanceLevel      :number,
  difficulty             :number,
  typicalLearningTime    :number,
  learningPlatform       :string,
  location               :string,
  author                 :string,
  language               :string,
  generative             :Boolean
}

export interface FrameworkNodeBD {
  name                :string,
  type                :string,
  mastery             :number, 
  trust               :number,
  cover               :number, 
  relations           :FrameworkNodeRelation,

  [key :string] :(string | FrameworkNodeRelation | Array<string> | number)
}

export interface FrameworkNodeRelation {
  isSkillOf                   :Array<string>,
  isKnowledgeOf               :Array<string>, 
  composes                    :Array<string>,
  hasSkill                    :Array<string>,
  hasKnowledge                :Array<string>, 
  isComposedOf                :Array<string>,
  requires?                   :Array<string>,
  complexifies?               :Array<string>,
  isComplexificationOf?       :Array<string>,
  isLeverOfUnderstandingOf?   :Array<string>,
  facilitatesUnderstandingOf? :Array<string>,
  comprises?                  :Array<string>,
  isComprisedIn?              :Array<string>,
  hasLearning?                :Array<string>,
  hasTraining?                :Array<string>,
  isRequiredBy?               :Array<string>,

  [key :string] :Array<string>
}

export async function requestProfilAPI(fwId :string, username :string) :Promise<FrameworkObjectBD> {
  try {
    return await fetchProfil(fwId, username);
  } catch(error) {
      console.log("Error fetching remote HTML: ", error);
  } 
}

/**
 * @returns a promise of the profil of the student. 
 */
const fetchProfil = async (fwId :string, username :string): Promise<FrameworkObjectBD> => {

  //for test --> TO ERASE
  const url :string = 'http://127.0.0.1:5000';
  
  //real url. Currently, need to get the authorization here before request:
  //https://cors-anywhere.herokuapp.com/corsdemo
  //const url :string = 'https://cors-anywhere.herokuapp.com/https://comper.projet.liris.cnrs.fr/sites/profile-engine';

  const route :string = '/api/profile/recommendation/index.php';

  const parameter1 :string = '?frameworkId=' + fwId;
  
  const parameter2 :string = '&learnerUsername=' + username;

  const parameters = parameter1 + parameter2;

  const api :string = url + route + parameters;
  
  var myHeaders = new Headers();
  myHeaders.append("Access-Control-Allow-Origin", "*");
  myHeaders.append("Access-Control-Allow-Methods", "POST, GET");
  myHeaders.append("Access-Control-Allow-Headers", "x-comper-accepted-host");
  myHeaders.append("x-comper-accepted-host", "https://traffic.irit.fr");

  var myInit = { method: 'GET',
               headers: myHeaders};

  var myRequest = new Request(api, myInit);
  
  try {
      const response = await fetch(myRequest, myInit)
      const data = await response.json()
      return data;
  } catch (error) {
      if (error) {
          return error.message
      }
  }
}