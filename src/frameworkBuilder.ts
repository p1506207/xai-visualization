import { Framework, PathNode, FrameworkNode, Relation, isHierarchicalRelation, Tag, SelectionStrategie, TypeNode } from "./framework";
import { FrameworkObjectBD, FrameworkNodeBD, FrameworkNodeRelation, FrameworkResourceBD, requestProfilAPI } from "./profil";
import { Traces, requestTracesAPI } from "./traces";

export class FrameworkBuilder {
    
    // The framework object from base
    private frameworkObjectBD :FrameworkObjectBD;
        
    // The tree object
    private framework :Framework;

    // Numero of the objectif treated
    private objectifNum :number;

    constructor(objectifNum :number) {
        this.objectifNum = objectifNum;
    }

    /**
     * Build a framework including the initialisation of every nodes and relations 
     * @returns the framework object
     */
    public async build() :Promise<Framework> {    
        this.frameworkObjectBD = await requestProfilAPI('83', 'asker:ext_Benoit.Riandiere');
        this.framework = new Framework(this.frameworkObjectBD.name);

        // Get every nodes of the framework
        let frameworkNodesBD :Array<FrameworkNodeBD> = [];
        this.frameworkObjectBD.objects.forEach((frameworknodeBD :FrameworkNodeBD) => {
            frameworknodeBD.name = this.decode(frameworknodeBD.name);
            frameworkNodesBD.push(frameworknodeBD);
        });

        // Map<id, resource> of resources
        let frameworkResourcesBD :Map<string, FrameworkResourceBD> = new Map<string, FrameworkResourceBD>();

        // Array<name> of nodes already created
        let createdNodes :Array<string> = [];

        // Map<name, Array<id>> which refered all nodes instancied for the name
        let duplicataOfNodes :Map<string, Array<string>> = new Map<string, Array<string>>();

        // Map<id, Array<id>> which refered all resources instancied for the id
        let duplicataOfResources :Map<string, Array<string>> = new Map<string, Array<string>>();
        this.frameworkObjectBD.resources.forEach((frameworkResourceBD :FrameworkResourceBD) => {
            frameworkResourceBD.name = this.decode(frameworkResourceBD.name);
            frameworkResourcesBD.set(frameworkResourceBD.id, frameworkResourceBD);
            duplicataOfResources.set(frameworkResourceBD.id, []);
        });

        // Map<name, Array<id>> which refered all parents already linked for each node
        let linkedParents :Map<string, Array<string>> = new Map<string, Array<string>>();

        let root :FrameworkNode = new FrameworkNode(this.generateId(), this.frameworkObjectBD.name, null, TypeNode.root, null, null);
        this.framework.addNode(root);
        createdNodes.push(root.getName());
        duplicataOfNodes.set(root.getName(), [root.getId()]);

        // Increment var for the while loop below
        let i  :number = 0;
        let antibug :number = 0;
        while (frameworkNodesBD.length > 0) {

            antibug++;
            if (antibug > 1000)
                break;

            // "increment" will be False if we removed an element for fw, because 'i' will be on the new current node position.
            let increment :boolean         = true;
            let node      :FrameworkNodeBD = frameworkNodesBD[i];
            let parents   :Array<string>   = node.relations.isSkillOf.concat(node.relations.isKnowledgeOf).concat(node.relations.composes);

            for (let i=0; i<parents.length; i++) {
                parents[i] = this.decode(parents[i]);
            }

            // For the first level nodes, we simply link them to the root of the tree.
            if (parents.length === 0) {
                let frameworkNode :FrameworkNode = new FrameworkNode(this.generateId(), node.name, root, this.getTypeNode(node.type), node.mastery, node.trust);
                frameworkNode.getParent().addChild(frameworkNode.getId());
                this.framework.addNode(frameworkNode);
                createdNodes.push(frameworkNode.getName());
                duplicataOfNodes.set(frameworkNode.getName(), [frameworkNode.getId()]);

                frameworkNodesBD.splice(i, 1);
                increment = false;
            }
            else {
                // Adds the "linkedParents" names to the node, so if we have to loop again on the node, we know which 
                // node<->parentNode link has been traited or not.
                if (linkedParents.get(node.name) === undefined) linkedParents.set(node.name, []);

                if (duplicataOfNodes.get(node.name) === undefined) duplicataOfNodes.set(node.name, []);
                parents.forEach((parentName :string) => {

                    if (linkedParents.get(node.name).indexOf(parentName) < 0) {
                        
                        if (createdNodes.indexOf(parentName) > -1) {
                            duplicataOfNodes.get(parentName).forEach((parentDuplicataId :string) => {
                                let parent :FrameworkNode = this.framework.getFrameworkNodes().get(parentDuplicataId);
                                let frameworkNode :FrameworkNode = new FrameworkNode(this.generateId(), node.name, parent, this.getTypeNode(node.type), node.mastery, node.trust);
                                parent.addChild(frameworkNode.getId());
                                this.framework.addNode(frameworkNode);
                                duplicataOfNodes.get(node.name).push(frameworkNode.getId());
                                if (linkedParents.get(node.name).indexOf(parentName) < 0)
                                    linkedParents.get(node.name).push(parentName);
                            });
                        }
                    }
                });

                // If we traited all the node<->parentNode links, we can remove the node from the framework and add resources
                if (linkedParents.get(node.name).length === parents.length) {
                    let learning :Array<string> = node.relations.hasLearning;

                    if (learning.length !== 0) {
                        duplicataOfNodes.get(node.name).forEach((nodeDuplicataId :string) => {
                            learning.forEach((resourceId :string) => {
                                let parent :FrameworkNode = this.framework.getFrameworkNodes().get(nodeDuplicataId);
                                let frameworkResource :FrameworkNode = new FrameworkNode(this.generateId(), frameworkResourcesBD.get(resourceId).name, parent, this.getTypeNode("Learning"), null, null);
                                parent.addChild(frameworkResource.getId());
                                this.framework.addNode(frameworkResource);
                                duplicataOfResources.get(resourceId).push(frameworkResource.getId());
                            });
                        });
                    }

                    let training :Array<string> = node.relations.hasTraining;

                    if (training.length !== 0) {
                        duplicataOfNodes.get(node.name).forEach((nodeDuplicataId :string) => {
                            training.forEach((resourceId :string) => {
                                let parent :FrameworkNode = this.framework.getFrameworkNodes().get(nodeDuplicataId);
                                let frameworkResource :FrameworkNode = new FrameworkNode(this.generateId(), frameworkResourcesBD.get(resourceId).name, parent, this.getTypeNode("Training"), null, null);
                                parent.addChild(frameworkResource.getId());
                                this.framework.addNode(frameworkResource);
                                duplicataOfResources.get(resourceId).push(frameworkResource.getId());
                            });
                        });
                    }

                    createdNodes.push(node.name);
                    frameworkNodesBD.splice(i, 1);
                    increment = false;
                }
            }

            // Increments or resets until we're finished with the nodes.
            if (increment) i += 1;
            if (i >= frameworkNodesBD.length) i = 0;
        }
        
        let file :string = await requestTracesAPI()
        
        let traces :Traces = new Traces(file, this.objectifNum);

        this.framework.setObjectifNb(traces.blocObjectif.length);

        // Uncomment to see logs get from the API
        //console.log("-blocReferentiel-");
        //console.log(traces.blocReferentiel);
        //console.log("-blocObjectif-");
        //console.log(traces.blocObjectif);
        //console.log("-blocSelectionKSC-");
        //console.log(traces.blocsSelectionKSC.get(this.objectifNum));
        //console.log("-blocUnicity-");
        //console.log(traces.blocUnicity);
        //console.log("-blocOrdering-");
        //console.log(traces.blocOrdering);
        //console.log("-blocSelectionResources-");
        //console.log(traces.blocSelectionResources);
        //console.log("-blocOrderingResources-");
        //console.log(traces.blocOrderingResources);
        //console.log("-blocRestricting-");
        //console.log(traces.blocRestricting);
        //console.log("-blocOut-");
        //console.log(traces.blocOut);

        this.framework.setObjectif(this.framework.getFrameworkNodes().get(duplicataOfNodes.get(traces.getObjectif())[0]));
        
        // Array<Array<[name, Relation, tag, strat]>> which contains every paths of recommandations
        let recommandationPaths :Array<Array<[string, Relation, Tag, SelectionStrategie]>> = traces.getRecommandationsPath();
        
        recommandationPaths.forEach(path => {
            
            // Set default value for every node about if they got a child in the path
            this.framework.getFrameworkNodes().forEach((node :FrameworkNode) => {
                node.hasChildOnPath.set(path[0][0], false);
            });

            // Keep parent for specify after with the id of the recommandation that they are in his path
            let parentInPath :Array<FrameworkNode> = [];

            // The path to objectif of node without resource
            let nodePath :Array<PathNode> = [];

            // Use for get the good child
            let lastParent :FrameworkNode = null;

            // We break before 0 to don't add the resource in the path
            for (let i :number = path.length - 1; i > 0; i--) {
                // For the objectif or not hierarchical children
                // we just get the first one we find
                if (i === path.length - 1 || !isHierarchicalRelation(path[i+1][1]) ) {
                    lastParent = this.framework.getFrameworkNodes().get(duplicataOfNodes.get(path[i][0])[0]);

                    // Add parent in list of parent
                    parentInPath.push(lastParent);
                    
                    nodePath.push(new PathNode(lastParent, path[i][1], path[i][2], path[i][3]));
                }
                // Else we need to find the good child
                else {
                    let nodeInPath :FrameworkNode = null;

                    lastParent.getChildren().forEach((childId :string) => {
                        let child :FrameworkNode = this.framework.getFrameworkNodes().get(childId);
                        if (child.getName() === path[i][0]) {
                            nodeInPath = child;
                        }
                    });

                    lastParent = nodeInPath;

                    // Add parent in list of parent
                    parentInPath.push(lastParent);

                    nodePath.push(new PathNode(lastParent, path[i][1], path[i][2], path[i][3]));
                }

            }

            // Reverse to get the path from the recommandation to the objectif
            nodePath.reverse();

            // Get the good resource depending if his parents
            let recommandationId :string = null;
            lastParent.getChildren().forEach((childId :string) => {
                let child :FrameworkNode = this.framework.getFrameworkNodes().get(childId);
                if (child.getName() === frameworkResourcesBD.get(path[0][0]).name) {
                    recommandationId = child.getId();
                }
            });

            this.addRecommandation(recommandationId, nodePath);    
            
            // Set variable to display node in the path on selection
            parentInPath.forEach((parent :FrameworkNode) => {
                this.setParentgotChildOnPath(parent, recommandationId);
            });
        })

        return this.framework;
    }

    /**
     * Add a recommandation in the tree and give the information to his parents
     * @param frameworkTree
     * @param recommandation the node recommanded
     */
    private addRecommandation(resourceId :string, path :Array<PathNode>) :void {
        let recommandationNode = this.framework.getFrameworkNodes().get(resourceId);
        this.framework.addRecommandedNode(recommandationNode, path);
        let parent :FrameworkNode = recommandationNode.getParent();
        while (parent !== undefined && parent !== null) {
            parent.setHasRecommandedChild(true);
            parent = parent.getParent();
        }    
    }

    /**
     * Return a new id
     */
    private generateId() :string {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < 6; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return Date.now()+result;
    }

    /**
     * Return the string in parameter in UTF8 encoding
     */
    private decode(encriptString :string) :string {
        return decodeURIComponent(escape(encriptString));
        // To replace if URI malformed error by
        // return encriptString;
    }
    
    /**
     * Return the type of node corresponding to the string
     */
    private getTypeNode(type :string) :TypeNode {
        switch(type) {
            case ("Competency"):
                return TypeNode.competency;
            case ("Knowledge"):
                return TypeNode.knowledge;
            case ("Skills"):
                return TypeNode.skill;
            case ("Learning"):
                return TypeNode.learning;
            case ("Training"):
                return TypeNode.training;
            default:
                return null;
        }
    }

    /**
     * Specify to every parent they got a child in the path of the recommandation
     */
    private setParentgotChildOnPath(childNode :FrameworkNode, recommandationId :string) :void {
        childNode.hasChildOnPath.set(recommandationId, true);

        let parent :FrameworkNode = childNode.getParent();

        while (parent !== null) {
            parent.hasChildOnPath.set(recommandationId, true);
            parent = parent.getParent();
        }
    }
}