import { Framework, PathNode, FrameworkNode, Relation, isHierarchicalRelation, Tag, SelectionStrategie, TypeNode, ClassNode } from "./framework";

// Get the text to display in the tooltip depending of the relation 
export function getTooltipTextForArrow(childNode :FrameworkNode, parentNode :FrameworkNode, relation :Relation, childPathNode :PathNode, linkColor :string) :string {
    let tooltipText :string = '';
    
    // Update of the content of the tooltip
    let childName :string = formatNodeName(childNode.getName()).toLowerCase();
    let parentName :string = formatNodeName(parentNode.getName()).toLowerCase();

    // Fix the synthax error "de le"
    let transitionText :string = '';
    let objectifText :string = '';
    if (parentNode.getClassNode() === ClassNode.objectif)
        objectifText = 'objectif'
    if (!childNode.isRessource() && (parentNode.getTypeNode() === TypeNode.skill || parentNode.getTypeNode() === TypeNode.learning )) {
        let textForRelation :string = getTooltipTextForRelation(relation);
        transitionText = textForRelation.substring(0, textForRelation.length - 1)
            + 'u</span> '
            + getTooltipTextForTypeNode(parentNode.getTypeNode()).substring(3)
            + ' '
            + objectifText
            + '<i style="font-weight:100;"> ' + parentName + '</i>.</div>';
    }
    else {
        transitionText = getTooltipTextForRelation(relation)
            + '</span> '
            + getTooltipTextForTypeNode(parentNode.getTypeNode())
            + ' '
            + objectifText
            + '<i style="font-weight:100;"> ' + parentName + '</i>.</div>';
    }

    // Make the first letter to uppercase
    let childType :string = getTooltipTextForTypeNode(childNode.getTypeNode());
    let childTypeWithMaj :string = childType[0].toUpperCase() + childType.substr(1)

    tooltipText = '<div>'
            + childTypeWithMaj
            + '<i style="font-weight:100;"> ' + childName + '</i> '
            + '<span style="color:' + linkColor +';font-weight:900;"> ' 
            + transitionText;
    
    // Display the cause of the selection of the node if the node isn't a resource
    if (!childNode.isRessource()) {
        tooltipText += getCauseOfSelection(childPathNode, childNode);
    }
    return tooltipText;
}

// Get the text to display in the second part of the tooltip depending of the selection strategie
// For more information about this, check the details of strategie.
function getCauseOfSelection(childPathNode :PathNode, childNode :FrameworkNode) :string {
    let isFemaleType :Boolean = isFemaleTypeNode(childNode.getTypeNode());
    
    let tooltipText :string = '</br><div>'
            + getTooltipText2ForTypeNode(childNode.getTypeNode())
            + ' vous est recommandé';
    
    if (isFemaleType)
        tooltipText += 'e';
    tooltipText += ' car '

    switch(childPathNode.getSelectionStrategie()) {
        case (SelectionStrategie.RS1):
            if (childNode.getTrust() > 0) {
                tooltipText += 'vous ne l'
                if (isFemaleType)
                    tooltipText += 'a';
                else
                    tooltipText += 'e';
                tooltipText += ' maîtrisez pas encore suffisamment';
            } else {
                tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                if (isFemaleType)
                    tooltipText += 'e';
            }
            break;
        case (SelectionStrategie.RS2):
            if (childNode.getTrust() > 0) {
                tooltipText += 'vous ne l'
                if (isFemaleType)
                    tooltipText += 'a';
                else
                    tooltipText += 'e';
                tooltipText += ' maîtrisez pas encore suffisamment';
            } else {
                tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                if (isFemaleType)
                    tooltipText += 'e';
            }
            break;
        case (SelectionStrategie.RS3):
            if (childNode.getTrust() > 0) {
                if (childPathNode.getTag() === Tag.revision) {
                    tooltipText += 'vous ne l'
                    if (isFemaleType)
                        tooltipText += 'a';
                    else
                        tooltipText += 'e';
                    tooltipText += ' maîtrisez pas encore suffisamment';
                }
                else {
                    tooltipText += 'cela peut vous aider à réviser'
                }
            } else {
                tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                if (isFemaleType)
                    tooltipText += 'e';
            }
            break;
        case (SelectionStrategie.RS4):
            // It shouldn't be true
            if (childNode.getTrust() > 0) {
                tooltipText += 'vous ne l'
                if (isFemaleType)
                    tooltipText += 'a';
                else
                    tooltipText += 'e';
                tooltipText += ' maîtrisez pas encore suffisamment';
            } else {
                tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                if (isFemaleType)
                    tooltipText += 'e';
            }
            break;
        case (SelectionStrategie.RS5):
            if (childNode.getTrust() > 0) {
                tooltipText += 'vous ne l'
                if (isFemaleType)
                    tooltipText += 'a';
                else
                    tooltipText += 'e';
                tooltipText += ' maîtrisez pas encore suffisamment';
            } else {
                tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                if (isFemaleType)
                    tooltipText += 'e';
            }
            break;
        // It shouldn't appeared
        case (SelectionStrategie.RS6):
            if (childNode.getTrust() > 0) {
                tooltipText += 'vous ne l'
                if (isFemaleType)
                    tooltipText += 'a';
                else
                    tooltipText += 'e';
                tooltipText += ' maîtrisez pas encore suffisamment';
            } else {
                tooltipText += 'vous ne l\'avez pas encore suffisamment travaillé';
                if (isFemaleType)
                    tooltipText += 'e';
            }
            break;
    }

    tooltipText += '.</div>';

    return tooltipText;
}

// Get the text to display in the tooltip depending of the relation 
function getTooltipTextForRelation(relation :Relation) :string {
    switch(relation) {
        case (Relation.hasSkill):
        case (Relation.isSkillOf):
        case (Relation.hasKnowledge):
        case (Relation.isKnowledgeOf):
        case (Relation.composes):
        case (Relation.isComposedOf):
        case (Relation.comprises):
        case (Relation.isComprisedIn):
            return 'est un composant de';
            
        case (Relation.complexifies):
        case (Relation.isComplexificationOf):
            return 'est une complexification de';
            
        case (Relation.facilitatesUnderstandingOf):
        case (Relation.isLeverOfUnderstandingOf):
            return 'est un levier de compréhension de';
            
        case (Relation.requires):
        case (Relation.isRequiredBy):
            return 'est un prérequis de';
            
        case (Relation.hasLearning):
        case (Relation.isLearningOf):
        case (Relation.hasTraining):
        case (Relation.isTrainingOf):
            return 'permet de travailler';
            
        default:
            return relation;
    }
}

// Get the text to display in the tooltip depending of the type of the node 
function getTooltipTextForTypeNode(type :TypeNode) :string {
    switch(type) {
        case (TypeNode.competency):
            return "la compétence";
        case (TypeNode.knowledge):
            return "la connaissance";
        case (TypeNode.skill):
            return "le savoir-faire";
        case (TypeNode.learning):
            return "le cours";
        case (TypeNode.training):
            return "l\'exercice";
        default:
            return "";
    }
}

// Return true if the type is female gender for the synthax
function isFemaleTypeNode(type :TypeNode) :Boolean {
    switch(type) {
        case (TypeNode.competency):
        case (TypeNode.knowledge):
            return true;
        case (TypeNode.skill):
            return false;
        default:
            return false;
    }
}

// Get the text to display in the tooltip depending of the type of the node 
function getTooltipText2ForTypeNode(type :TypeNode) :string {
    switch(type) {
        case (TypeNode.competency):
            return "Cette compétence";
        case (TypeNode.knowledge):
            return "Cette connaissance";
        case (TypeNode.skill):
            return "Ce savoir-faire";
        default:
            return "";
    }
}

// Convert number to percentage
export function formatValue(value :number) :string {
    if(isNaN(value) || value === undefined || value === null) 
        value = 0;
    return Math.round(value * 100).toString()+'%';
}

// Convert name from BD to a propre text
export function formatNodeName(nodeName :string) :string {
    return formatResourceName(nodeName).replace(new RegExp("_", "g"), ' ')
            .replace(new RegExp("de une", "g"), 'd\'une')
            .replace(new RegExp("dune", "g"), 'd\'une')
            .replace(new RegExp("dun", "g"), 'd\'un')
            .replace(new RegExp("de un", "g"), 'd\'un');
}

//Exclude the prefix the name of the resource
function formatResourceName(oldName :string) :string {
    let newName :Array<string> = oldName.split(" - ", 2);
    return newName[newName.length-1];
}