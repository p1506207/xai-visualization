import { Relation, Tag, SelectionStrategie, getSymetricRelation } from "./framework";

export async function requestTracesAPI() :Promise<string> {
    try {
        return await fetchTraces();
    } catch(error) {
        console.log("Error fetching remote HTML: ", error);
    } 
}
  
/**
 * @returns a promise of the trace of recommandations. 
 */
const fetchTraces = async (): Promise<string> => {
    
    //for test --> TO ERASE
    const url :string = 'http://127.0.0.1:5000';

    const route :string = '/api/logs/last/';

    //const api = "https://traffic.irit.fr/comper/recommendations/api/logs/last/";

    const api :string = url + route;

    var myHeaders = new Headers();
    myHeaders.append("Access-Control-Allow-Origin", "*");
    myHeaders.append("Access-Control-Allow-Methods", "POST, GET");
    myHeaders.append("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYXNrZXI6ZXh0X0Jlbm9pdC5SaWFuZGllcmUiLCJyb2xlIjoibGVhcm5lciIsInVzZXJuYW1lIjoiZXh0X0Jlbm9pdC5SaWFuZGllcmUiLCJvYmplY3RpdmVzIjpbWyJNYW5pcHVsZXJfbGVzX3BhcmFtw6h0cmVzX2R1bl9zY3JpcHQiLCJSZXZpc2lvbiJdLFsiU3Vic3RpdHVlcl91bmVfY29tbWFuZGVfcGFyX3Nvbl9leMOpY3V0aW9uIiwiUmV2aXNpb24iXSxbIlLDqWFsaXNlcl9kZXNfY2FsY3Vsc19hcml0aG3DqXRpcXVlcyIsIlJldmlzaW9uIl1dLCJmd2lkIjo4MywiZXhwIjoyMCwiaWF0IjoxNjI0NjIyMzMwfQ.FlXyrS3KhdFqfZwo38Lo_g2ipIKCKxGLk4IFeIXmr2Ph2jBO0GrNnteH7tp-3wu6YvXGnwIOl0U9LMJxX64SCeBm5JEIyaTvpxWYQanG9fcIuV0GDmE8ZSQK_EOQ0VvdswkWAqHNd0ZaR2QqfQhZfMJ6ywE2CU2OPfcFk-fK-MbUM9x1x5eHvqr--KCn7PR-OIl3EcHmWtNFCW2OPjKu-WvVsdomrF_9dInT9VhKHcBZUSbfnujAtkmswdXQWdHlR6a8dqU2YrLEpYxwbk4AGjugvFm5AapGt-0suVHaemcoe-UEDMx95clrC38VUqKAfPoDIUeD2GijZc2u7vRNqDvh2QSlJOXYPbiPzBH83oc3kUm6bDEXPbISGeLEIpFgL0tOI7l-2lwHXLo12dpDyPdtZDIuxhaGvkXLHzWCeG3ZoX0CFc61QaTZJGr9ish1x-jZtihJAWmbzI0zsYAxgV62H21sHxBaEmIcbEaavI9-uaWjH_zA_y6U1pqvh_nJoQ3UZXtAsiXviUKnelE61WWda12IqhwSDwpMuk40WVUlDkxNRBTgx-AgJwy1Zl-QNdioU8tbXZ7uroNtUWNtXd8lw27GpgBUIK3tTh1xsHuLE6KXc9HfOa4jhQbS4gem9WRls4SgpysNbrhfaRsGim07CLE3mHDp-ss9EcYxLWk");
    myHeaders.append("Accept", "text/plain");

    var myInit = { method: 'GET',
                    headers: myHeaders};

    var myRequest = new Request(api, myInit);

    try {
        const response = await fetch(myRequest, myInit);
        const data :string = await response.text();
        return data;
    } catch (error) {
        if (error) {
            return error.message
        }
    }
}

/**
 * Contains data get from traces
 */
export class Traces {

    private objectif :string;

    private idObjectif :number;

    private recommandationsPath :Array<Array<[string, Relation, Tag, SelectionStrategie]>>;

    /**
     * Each bloc variable contains a list of string associate to the the bloc in the trace
     */
    public blocReferentiel :Array<string> = [];
    
    public blocProfil :Array<string> = [];

    public blocObjectif :Array<string> = [];

    public blocsSelectionKSC :Map<number, Array<string>> = new Map<number,Array<string>>();

    public blocUnicity :Array<string> = [];

    public blocOrdering :Array<string> = [];

    public blocSelectionResources :Array<string> = [];

    public blocOrderingResources :Array<string> = [];

    public blocRestricting :Array<string> = [];

    public blocOut :Array<string> = [];

    constructor(tracesFile :string, objectifNum :number) {
        this.setBlocs(tracesFile, objectifNum);
        this.setRecommandationsPath(objectifNum);
    }

    public getObjectif() :string {
        return this.objectif;
    }

    public getRecommandationsPath() :Array<Array<[string, Relation, Tag, SelectionStrategie]>> {
        return this.recommandationsPath;
    }

    /**
     * Use traces to generate paths between each recommandation and the objectif
     * @param objectifNum the objectif we want explanation
     */
    private setRecommandationsPath(objectifNum :number) :void {
        this.recommandationsPath = [];
        let path :Array<[string, Relation, Tag, SelectionStrategie]> = [];

        let blocSelectionKSC :Array<string> = this.blocsSelectionKSC.get(objectifNum);

        // For each recommandation
        this.blocOut.forEach((recommandation :string) => {
            let decompose :Array<string> = recommandation.split("'");
            let ids :Array<string> = decompose[2].substring(0, decompose[2].length - 1).split(",");
            
            let idRecommandation :string = decompose[1];
            let idParent :number = parseInt(ids[1].substring(1));
            let idObjectif :number = parseInt(ids[2].substring(1));

            // If it correspond to the objectif treated
            if (idObjectif === this.idObjectif) {

                path = [];
                path.push([idRecommandation, null, null, null]);

                // Find if the resource is an exercise or a course to get the appropriate relation
                let relation :string = "";
                this.blocOrderingResources.forEach((line :string) => {
                    if (line.includes(idRecommandation)) {
                        let link :string = line.split("'")[3];
                        if (link = "expositive")
                            relation = "hasLearning";
                        else
                            relation = "hasTraining";
                    }
                });

                // Iterate over the output of the selection phase to get the tag and weight
                let i :number = blocSelectionKSC.length - 1;
                let bestWeight :number = 0;
                let parentName :string = "";
                while (blocSelectionKSC[i].includes("Output")) {
                    let stringFragment :Array<string> = blocSelectionKSC[i].split("'");
                    let idNode :number = parseInt(stringFragment[4].substring(3, stringFragment[4].length - 2));

                    if (idNode === idParent) {
                        let weight :number = parseFloat(stringFragment[12].substring(2, stringFragment[12].length - 1));

                        if (weight > bestWeight) {
                            bestWeight = weight;
                            parentName = stringFragment[5];
                        }
                    }
                    i--;
                }

                // Iterate over the selection phase to get the path
                while (parentName !== this.objectif) {
                    if (i < 0) {
                        break;
                    }
                    if (blocSelectionKSC[i].includes("[treatment][") && blocSelectionKSC[i].includes("][selection]")) {
                        
                        let stringFragment :Array<string> = blocSelectionKSC[i].split("'");

                        // Check if it's the good selection of the parent
                        if (stringFragment[1] !== parentName && stringFragment[5] === parentName) {
                            // && parseFloat(stringFragment[8].substring(2, stringFragment[8].length - 2)) === bestWeight
                            
                            let selectionRule :string = blocSelectionKSC[i].split("][")[3];
                            let tag :string = stringFragment[11];
                            path.push([parentName, getRelation(relation), getTag(tag), getSelectionStrategie(selectionRule)]);

                            // Go to his selection to get his father and his relation with him
                            while (i >= 0) {
                                if (!blocSelectionKSC[i].includes("][preTreatment][output]")) {
                                    i--;
                                } else if (blocSelectionKSC[i].split("'")[5] !== parentName) {
                                    i--;
                                } else {
                                    break;
                                }
                            }
                            stringFragment = blocSelectionKSC[i].split("'");
                            relation = stringFragment[13];
                            parentName = stringFragment[17];

                            // Go to his selection to get his father and his relation with him
                            while (i >= 0 && blocSelectionKSC[i].split("'")[1] !== parentName) {
                                    stringFragment = blocSelectionKSC[i].split("'");
                                    if (blocSelectionKSC[i].includes("][initialisation]") && stringFragment[5] === parentName && blocSelectionKSC[i].includes("father")) {
                                        let selectionRule :string = blocSelectionKSC[i].split("][")[3];
                                        path.push([parentName, getRelation(relation), null, getSelectionStrategie(selectionRule)]);
                                        relation = Relation[getSymetricRelation(getRelation(stringFragment[13]))];
                                        parentName = stringFragment[17];
                                    }
                                    i--;
                            }
                        }
                    }
                    i--;
                }
                path.push([parentName, getRelation(relation), null, null]);
                this.recommandationsPath.push(path);
            }
        });
    }

    /**
     * Set bloc lines in class variables
     * @param tracesFile the traces just get from API
     * @param objectifNb the objectif we want explanation
     */
    private setBlocs(tracesFile :string, objectifNum :number) {
        let tracesLines :Array<string> = tracesFile.split("\n");
        let line = 0;

        let nbObjectif :number = 0;
        let currentObjectif :number = 1;

        // Browse the trace file
        while (line < tracesLines.length) {

            // blank
            if (tracesLines[line] === "\n" && this.blocObjectif.length !== 0 && this.blocsSelectionKSC.get(currentObjectif).length !== 0 )
                currentObjectif++;

            // bloc : framework
            else if (tracesLines[line].includes("[Referential]"))
                this.blocReferentiel.push(tracesLines[line]);

            // bloc : profil
            else if (tracesLines[line].includes("[Profil]"))
                this.blocProfil.push(tracesLines[line]);

            // bloc : Objectif
            else if (tracesLines[line].includes("[Objective]")) {
                this.blocObjectif.push(tracesLines[line]);
                nbObjectif++;
                this.blocsSelectionKSC.set(nbObjectif, []);

            // blocs : Selection of KSC
            } else if (tracesLines[line].includes("[Selection][KSC]")) {
                if (tracesLines[line].includes("[Unicity]"))
                    this.blocUnicity.push(tracesLines[line]);
                else
                    this.blocsSelectionKSC.get(currentObjectif).push(tracesLines[line]);

            // blocs : Ordering
            } else if (tracesLines[line].includes("[Ordering][KSC]"))
                this.blocOrdering.push(tracesLines[line]);

            // blocs : Selection resources
            else if (tracesLines[line].includes("[Selection][resources]"))
                this.blocSelectionResources.push(tracesLines[line]);

            // blocs : Ordering resources
            else if (tracesLines[line].includes("[Ordering][resources]"))
                this.blocOrderingResources.push(tracesLines[line]);

            // blocs : Restricting
            else if (tracesLines[line].includes("[Restricting][resources]")) {

                if (tracesLines[line].includes("[out]"))
                    this.blocOut.push(tracesLines[line]);
                else
                    this.blocRestricting.push(tracesLines[line]);
            }
            line++;
        }

        // Set the objectif depending of the objectif number
        this.objectif = this.blocObjectif[objectifNum - 1].split("'")[5];

        let idFragment :string = this.blocObjectif[objectifNum - 1].split("'")[4];
        this.idObjectif = parseInt(idFragment.substring(3, idFragment.length - 2));
    }
}

function getRelation(relation :string) :Relation {
    let key :keyof typeof Relation = relation as keyof typeof Relation;
    return Relation[key];
}

function getTag(tag :string) :Tag {
    switch(tag) {
        case ("Tag_prerequis"):
            return Tag.prerequis;
        case ("Tag_remediation"):
            return Tag.remediation;
        case ("Tag_renforcement"):
            return Tag.renforcement;
        case ("Tag_revision"):
            return Tag.revision;
        case ("Tag_decouverte"):
            return Tag.découverte;
        case ("Tag_approfondissement"):
            return Tag.approfondissement;
        case ("Tag_a_travailler"):
            return Tag.aTravailler;
        default:
            return null;
    }
}

function getSelectionStrategie(selectionRule :string) :SelectionStrategie {
    switch(selectionRule) {
        case ("SP1"):
            return SelectionStrategie.RS1;
        case ("SP2"):
            return SelectionStrategie.RS2;
        case ("SP3"):
            return SelectionStrategie.RS3;
        case ("SP4"):
            return SelectionStrategie.RS4;
        case ("SP5"):
            return SelectionStrategie.RS5;
        case ("SP6"):
            return SelectionStrategie.RS6;
        default:
            return null;
    }
}